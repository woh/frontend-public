/* eslint-disable no-unused-vars */
// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
// import 'es6-promise/auto';

import Vue from 'vue';

import VueFroala from 'vue-froala-wysiwyg';
import 'froala-editor/js/froala_editor.pkgd.min';
import 'froala-editor/js/languages/ru';
import 'froala-editor/js/plugins.pkgd.min';

import linkify from 'vue-linkify';
import vueHeadful from 'vue-headful';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import 'element-ui/lib/theme-chalk/display.css';
import 'element-ui/lib/theme-chalk/base.css';
import VueSelectImage from 'vue-select-image';
import Croppa from 'vue-croppa';
import 'vue-croppa/dist/vue-croppa.css';
import axios from 'axios';
import Avatar from 'vue-avatar';
import moment from 'moment';
import lang from 'element-ui/lib/locale/lang/ru-RU';
import locale from 'element-ui/lib/locale';
import App from './App';
import router from './router';

import NewsLayout from './layouts/NewsLayout';
import WowLayout from './layouts/WowLayout';
import PostTagsAdd from './components/parts/PostTagsAdd';
import PostTags from './components/posts/parts/PostTags';
import PostComments from './components/comments/CommentsList';
import Loading from './components/parts/misc/Loading';
import Comments from './components/comments/Comment';
import PostLike from './components/posts/parts/PostLike';
import CommentLike from './components/comments/CommentLike';
import MobileWidgetHashtag from './components/parts/MobileWidgetHashtag';
import WidgetHashtag from './components/parts/WidgetHashtag';
import PostCategoryAdd from './components/parts/PostCategoryAdd';
import PostCategories from './components/posts/parts/PostCategories';
import Post from './components/posts/Post';
import store from './store';
import './assets/js/fitie';
import './assets/css/foundation.css';
import './assets/css/hamburgers.css';
import './assets/css/custom-theme.css';
import './assets/css/woh.css';
import './assets/css/all.css';
import './assets/css/normalize.css';
import './assets/css/jquery.Jcrop.css';
import './assets/css/wowhead.css';

// Require Froala Editor js file.
require('froala-editor/js/froala_editor.min.js');
// Require Froala Editor css files.
require('froala-editor/css/froala_editor.pkgd.min.css');
require('froala-editor/css/froala_style.min.css');
require('froala-editor/css/third_party/embedly.min.css');
require('vue-select-image/dist/vue-select-image.css');

Vue.use(ElementUI);
Vue.use(VueFroala);
Vue.use(Croppa);
locale.use(lang);

moment.locale('ru');
Vue.filter('date', (value) => {
  if (!value) return value;
  return moment(String(value)).format('L LTS');
});

Vue.filter('dateShort', (value) => {
  if (!value) return value;
  return moment(String(value)).format('L');
});

Vue.filter('formatDate', (value) => {
  if (!value) return value;
  return moment(String(value)).calendar();
});

Vue.directive('linkified', linkify);
Vue.directive('scroll', {
  inserted(el, binding) {
    const f = function ins(evt) {
      if (binding.value(evt, el)) {
        window.removeEventListener('scroll', f);
      }
    };
    window.addEventListener('scroll', f);
  },
});

Vue.directive('focus', {
  inserted(el) {
    el.focus();
  },
});

Vue.component('NewsLayout', NewsLayout);
Vue.component('WowLayout', WowLayout);
Vue.component('PostTagsAdd', PostTagsAdd);
Vue.component('PostTags', PostTags);
Vue.component('PostComments', PostComments);
Vue.component('Loading', Loading);
Vue.component('Comments', Comments);
Vue.component('PostLike', PostLike);
Vue.component('CommentLike', CommentLike);
Vue.component('Post', Post);
Vue.component('Avatar', Avatar);
Vue.component('vue-headful', vueHeadful);
Vue.component('MobileWidgetHashtag', MobileWidgetHashtag);
Vue.component('WidgetHashtag', WidgetHashtag);
Vue.component('PostCategoryAdd', PostCategoryAdd);
Vue.component('PostCategories', PostCategories);
Vue.component('VueSelectImage', VueSelectImage);

const token = localStorage.getItem('user-token');
if (token) {
  axios.defaults.headers.common.Authorization = `Bearer ${token}`;
}

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App },
});
