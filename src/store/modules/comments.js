/* eslint-disable no-shadow,no-param-reassign,no-unused-vars,no-console */
import axios from 'axios';
import {
  COMMENT_LIKE, COMMENT_DISLIKE, COMMENT_UPDATE, COMMENT_DELETE, COMMENT_SAVE, COMMENTS_GET,
  COMMENT_ADD_ERROR, COMMENT_ADD_REQUEST, COMMENT_ADD_SUCCESS, COMMENT_ADD_TEMPORARY, COMMENT_DELETE_TEMPORARY,
} from '../actions/comments';


const state = { comments: [], totalComments: null, commentAddStatus: null };

const getters = {
  comments: state => state.comments,
  totalComments: state => state.totalComments,
};

const actions = {
  [COMMENTS_GET]: ({ commit }, id) =>
    new Promise((resolve, reject) => {
      axios({ url: `${process.env.apiUrl}/${id}/comments/` })
        .then((response) => {
          commit(COMMENTS_GET, response);
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    }),
  [COMMENT_ADD_REQUEST]: ({ commit, dispatch }, newComment) =>
    new Promise((resolve, reject) => {
      let delay = 0;
      let count = 0;
      let commentAdd = setTimeout(function request() {
        axios({
          headers: { 'X-Query-Id': newComment.guid },
          url: `${process.env.apiUrl}/${newComment.postId}/comments`,
          data: newComment,
          method: 'POST',
        })
          .then((response) => {
            const comment = response.data[0];
            comment.guid = newComment.guid;
            comment.status = 'success';
            commit(COMMENT_ADD_SUCCESS, comment);
            console.log('новый коммент', comment);
            resolve(response);
          })
          .catch((err) => {
            if ((err.status >= 500 || !navigator.onLine) && count < 6) {
              if (count === 4) {
                delay = 3000;
              } else if (count === 5) {
                delay = 5000;
              }
              count += 1;
              commentAdd = setTimeout(request, delay);
            } else if (count === 6) {
              const error = Object.assign({}, newComment);
              console.log(error);
              commit(COMMENT_ADD_ERROR, error);
            }
            reject(err);
          });
      }, delay);
    }),
  [COMMENT_DELETE]: ({ commit, dispatch }, comment) =>
    new Promise((reject) => {
      axios({ url: `${process.env.apiUrl}/${comment.postId}/comments/delete/${comment.id}`, method: 'POST' })
        .then(() => {
          dispatch(COMMENTS_GET, comment.postId);
        })
        .catch((err) => {
          console.log('Error :-S', err);
          reject(err);
        });
    }),
  [COMMENT_SAVE]: ({ commit, dispatch }, comment) =>
    new Promise((resolve, reject) => {
      axios({ url: `${process.env.apiUrl}/${comment.postId}/comments/edit/`, data: comment, method: 'POST' })
        .then((response) => {
          console.log(response);
          commit(COMMENT_UPDATE, response.data);
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    }),
  [COMMENT_LIKE]: ({ commit, dispatch }, comment) =>
    new Promise((resolve, reject) => {
      axios({ url: `${process.env.apiUrl}/${comment.postId}/comments/like/${comment.id}/`, method: 'POST' })
        .then((response) => {
          commit(COMMENT_UPDATE, response.data);
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    }),
  [COMMENT_DISLIKE]: ({ commit, dispatch }, comment) =>
    new Promise((resolve, reject) => {
      axios({ url: `${process.env.apiUrl}/${comment.postId}/comments/dislike/${comment.id}/`, method: 'POST' })
        .then((response) => {
          commit(COMMENT_UPDATE, response.data);
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    }),
};

const mutations = {
  [COMMENT_ADD_TEMPORARY]: (state, temporary) => {
    temporary.id = temporary.guid;
    temporary.status = 'sending';
    state.comments.unshift(temporary);
  },
  [COMMENT_DELETE_TEMPORARY]: (state, index) => {
    state.comments.splice(index, 1);
  },
  [COMMENT_ADD_SUCCESS]: (state, comment) => {
    state.comments = state.comments.map(item => (item.guid === comment.guid ? comment : item));
  },
  [COMMENT_ADD_ERROR]: (state, comment) => {
    state.comments.forEach((item, i) => {
      if (item.guid === comment.guid) {
        state.comments[i].status = 'error';
      }
    });
  },
  [COMMENTS_GET]: (state, response) => {
    state.comments = response.data;
    state.totalComments = response.data.length;
  },
  [COMMENT_UPDATE]: (state, response) => {
    state.comments = state.comments.map(item => (item.id === response.id ? response : item));
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
