/* eslint-disable no-shadow,no-param-reassign,no-unused-vars,no-console */
import Vue from 'vue';
import axios from 'axios';
import {
  USER_REQUEST, USER_ERROR, USER_SUCCESS,
  AVATAR_SAVE, AVATAR_DEL, USER_SAVE, USER_SAVE_PASS,
} from '../actions/user';
import { AUTH_LOGOUT } from '../actions/auth';

const state = { status: '', user: {}, userAvatar: '' };

const getters = {
  user: state => state.user,
  isProfileLoaded: state => !!state.user.email,
  userAvatar: state => state.userAvatar,
};

const actions = {

  [USER_REQUEST]: ({ commit, dispatch }) => {
    commit(USER_REQUEST);
    axios({ url: `${process.env.apiUrl}/user`, method: 'GET' })
      .then((response) => {
        commit(USER_SUCCESS, response);
      })
      .catch((resp) => {
        commit(USER_ERROR);
        // if resp is unauthorized, logout, to
        commit(AUTH_LOGOUT);
        dispatch(AUTH_LOGOUT);
      });
  },
  [AVATAR_SAVE]: ({ commit, dispatch }, avatar) => {
    axios({ url: `${process.env.apiUrl}/user/avatar/`, data: avatar, method: 'POST' })
      .then((resp) => {
        commit(AVATAR_SAVE, resp);
      })
      .catch((err) => {
      });
  },

  [AVATAR_DEL]: ({ commit, dispatch }) => {
    axios({ url: `${process.env.apiUrl}/user/avatar/drop/`, method: 'POST' })
      .then((resp) => {
        dispatch(USER_REQUEST);
      })
      .catch((err) => {
      });
  },

  [USER_SAVE]: ({ commit, dispatch }, user) => {
    axios({ url: `${process.env.apiUrl}/user/save/`, data: user, method: 'POST' })
      .then((response) => {
        commit(USER_SUCCESS, response);
      })
      .catch((err) => {
      });
  },
  [USER_SAVE_PASS]: ({ commit, dispatch }, pass) => {
    axios({ url: `${process.env.apiUrl}/user/password`, data: pass, method: 'POST' })
      .then((response) => {
        commit(USER_SAVE_PASS, response);
      })
      .catch((err) => {
      });
  },

};

const mutations = {
  [USER_REQUEST]: (state) => {
    state.status = 'loading';
  },
  [USER_SUCCESS]: (state, response) => {
    const user = response.data;
    if (user.avatar) {
      user.avatar = `${process.env.imgRoot}${user.avatar}`;
    }
    user.isMailConfirmed = user.isMailConfirmed || false;
    user.access = user.role === 'ROLE_ADMIN' || user.role === 'ROLE_MODER';
    Vue.set(state, 'user', user);
    state.status = 'success';
  },
  [USER_ERROR]: (state) => {
    state.status = 'error';
  },
  [AUTH_LOGOUT]: (state) => {
    state.user = {};
  },
  [AVATAR_SAVE]: (state, response) => {
    state.user.avatar = `${process.env.imgRoot}${response.data}`;
  },
  [USER_SAVE]: (state, response) => {
  },
  [USER_SAVE_PASS]: (state, response) => {
    console.log(response);
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
