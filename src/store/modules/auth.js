/* eslint-disable promise/param-names,no-shadow,no-param-reassign,
no-unused-vars,no-console,prefer-destructuring */
import Vue from 'vue';
import axios from 'axios';
import { AUTH_REQUEST, AUTH_ERROR, AUTH_SUCCESS, AUTH_LOGOUT, AUTH_REGISTRATION, AUTH_REG_DONE, AUTH_REG_ERROR, AUTH_SHOW } from '../actions/auth';
import { USER_REQUEST } from '../actions/user';

const state = {
  token: localStorage.getItem('user-token') || '',
  status: '',
  regStatus: '',
  authorizeShow: false,
};

const getters = {
  isAuthenticated: state => !!state.token,
  authStatus: state => state.status,
  regStatus: state => state.regStatus,
  authorizeShow: state => state.authorizeShow,
};

const actions = {
  [AUTH_REQUEST]: ({ commit, dispatch }, user) =>
    new Promise((resolve, reject) => { // The Promise used for router redirect in authorize
      commit(AUTH_REQUEST);
      axios({ url: `${process.env.apiUrl}/user/login`, data: user, method: 'POST' })
        .then((resp) => {
          if (resp.data.role === 'ROLE_ADMIN' || resp.data.role === 'ROLE_MODER' || resp.data.role === 'ROLE_USER') {
            const token = resp.data.token;
            localStorage.setItem('user-token', token);
            axios.defaults.headers.common.Authorization = `Bearer ${token}`;
            commit(AUTH_SUCCESS, resp);
            dispatch(USER_REQUEST);
            resolve(resp);
          } else {
            commit(AUTH_ERROR);
            localStorage.removeItem('user-token');
          }
        })
        .catch((err) => {
          commit(AUTH_ERROR, err);
          localStorage.removeItem('user-token');
          reject(err);
        });
    }),

  [AUTH_REGISTRATION]: ({ commit, dispatch }, user) =>
    new Promise((resolve, reject) => {
      axios({ url: `${process.env.apiUrl}/user/register/`, data: user, method: 'POST' })
        .then((response) => {
          if (response.status === 201) {
            console.log(response);
            commit(AUTH_REG_DONE);
            dispatch(AUTH_REQUEST, user);
          }
          resolve(response);
        })
        .catch((err) => {
          if (err.response.status === 409 || err.response.status === 500) {
            commit(AUTH_REG_ERROR);
          }
          resolve(err);
        });
    }),

  [AUTH_LOGOUT]: ({ commit, dispatch }) => new Promise((resolve, reject) => {
    commit(AUTH_LOGOUT);
    localStorage.removeItem('user-token');
    delete axios.defaults.headers.common.Authorization;
    resolve();
  }),
};

const mutations = {
  [AUTH_REQUEST]: (state) => {
    state.status = 'loading';
  },
  [AUTH_SUCCESS]: (state, token) => {
    state.status = 'success';
    state.token = token;
  },
  [AUTH_ERROR]: (state) => {
    state.status = 'error';
  },
  [AUTH_LOGOUT]: (state) => {
    state.token = '';
    state.status = 'logout';
  },
  [AUTH_REG_DONE]: (state) => {
    state.regStatus = 'success';
  },
  [AUTH_REG_ERROR]: (state) => {
    state.regStatus = 'conflict';
  },
  [AUTH_SHOW]: (state) => {
    state.authorizeShow = state.authorizeShow === false;
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
