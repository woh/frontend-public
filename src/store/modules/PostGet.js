/* eslint-disable no-shadow,no-param-reassign,no-console */
import axios from 'axios';
import {
  POST_REQUEST, POST_SUCCESS, POSTS_REQUEST, POSTS_SUCCESS, POSTS_ERROR, POST_ERROR, POSTS_PURGE,
  PAGE_COUNT, POSTS_BY_TAG_REQUEST, POSTS_BY_TAG_SUCCESS, NEAREST, TAGS_PURGE, POSTS_BY_CATEGORY_REQUEST,
  POSTS_BY_CATEGORY_SUCCESS, CATEGORY_PURGE,
} from '../actions/PostGet';

const postsPrototype = {
  posts: [],
  pageCount: { totalPages: '', currentPage: '' },
  postsCount: '',
  postsLoading: false,
};

const state = {
  post: {},
  postStatus: '',
  nearest: [],

  posts: Object.assign({}, postsPrototype),
  postsByTag: Object.assign({}, postsPrototype),
  postsByCategory: Object.assign({}, postsPrototype),
};

const getters = {
  post: state => state.post,
  postStatus: state => state.postStatus,
  posts: state => state.posts,
  postsByTag: state => state.postsByTag,
  postsByCategory: state => state.postsByCategory,
  nearest: state => state.nearest,
};

const actions = {
  [POSTS_REQUEST]: ({ commit }, page) => {
    commit(POSTS_REQUEST);
    axios({
      url: `${process.env.apiUrl}/?page=${page}`,
      method: 'GET',
    })
      .then((response) => {
        commit(POSTS_SUCCESS, response);
      })
      .catch((err) => {
        commit(POSTS_ERROR);
        console.log('Error :-S', err);
      });
  },

  [POST_REQUEST]: ({ commit }, id) => {
    commit(POST_REQUEST);
    axios({ url: `${process.env.apiUrl}/${id}`, method: 'GET' })
      .then((response) => {
        commit(POST_SUCCESS, response);
      })
      .catch((err) => {
        commit(POST_ERROR);
        console.log('Error :-S', err);
      });
  },
  [POSTS_BY_CATEGORY_REQUEST]: ({ commit }, category) => {
    commit(POSTS_BY_CATEGORY_REQUEST);
    axios({ url: `${process.env.apiUrl}/by-category/${category.category}/?page=${category.page}`, method: 'GET' })
      .then((response) => {
        commit(POSTS_BY_CATEGORY_SUCCESS, response);
      })
      .catch((err) => {
        console.log('Error :-S', err);
      });
  },

  [POSTS_BY_TAG_REQUEST]: ({ commit }, tag) => {
    commit(POSTS_BY_TAG_REQUEST);
    axios({ url: `${process.env.apiUrl}/by-tag/${tag.tag}/?page=${tag.page}`, method: 'GET' })
      .then((response) => {
        commit(POSTS_BY_TAG_SUCCESS, response);
      })
      .catch((err) => {
        commit(POST_ERROR);
        console.log('Error :-S', err);
      });
  },

  [NEAREST]: ({ commit }, id) => {
    axios({ url: `${process.env.apiUrl}/${id}/nearest`, method: 'GET' })
      .then((response) => {
        commit(NEAREST, response);
      })
      .catch((err) => {
        console.log('Error :-S', err);
      });
  },
};

const mutations = {
  [POST_REQUEST]: (state) => {
    state.post = '';
    state.postStatus = 'loading';
  },
  [POST_SUCCESS]: (state, response) => {
    /* eslint-disable max-len */
    const re = '<p><span class="fr-video fr-fvc fr-dvb fr-draggable"' +
      ' contenteditable="false" draggable="true">' +
      '<iframe width="640" height="360" src="https://www.youtube.com/embed/$1?wmode=opaque" frameborder="0" ' +
      'allowfullscreen="" class="fr-draggable"></iframe></span></p>';
    response.data.text = response.data.text.replace(/https:\/\/www.youtube.com\/watch\?v=(.*?)$/g, re);
    state.post = response.data;
    state.postStatus = 'success';
  },
  [POST_ERROR]: (state) => {
    state.postStatus = 'error';
  },


  [POSTS_REQUEST]: (state) => {
    state.posts.postsLoading = true;
  },
  [POSTS_SUCCESS]: (state, response) => {
    state.posts.posts = state.posts.posts.concat(response.data.items);
    state.posts.postsCount = response.data.totalCount;
    state.posts.pageCount.totalPages = response.data.totalPages;
    state.posts.pageCount.currentPage = response.data.currentPage;
    state.posts.postsLoading = false;
  },
  [POSTS_ERROR]: (state) => {
    state.PostsStatus = 'error';
  },
  [POSTS_PURGE]: (state) => {
    state.posts.posts = [];
  },

  [POSTS_BY_TAG_REQUEST]: (state) => {
    state.postsByTag.postsLoading = true;
  },
  [POSTS_BY_TAG_SUCCESS]: (state, response) => {
    state.postsByTag.posts = state.postsByTag.posts.concat(response.data.items);
    state.postsByTag.postsCount = response.data.totalCount;
    state.postsByTag.pageCount.totalPages = response.data.totalPages;
    state.postsByTag.pageCount.currentPage = response.data.currentPage;
    state.postsByTag.postsLoading = false;
  },
  [TAGS_PURGE]: (state) => {
    state.postsByTag.posts = [];
  },

  [POSTS_BY_CATEGORY_REQUEST]: (state) => {
    state.postsByCategoryLoading = true;
  },
  [POSTS_BY_CATEGORY_SUCCESS]: (state, response) => {
    state.postsByCategory.posts = state.postsByCategory.posts.concat(response.data.items);
    state.postsByCategory.postsCount = response.data.totalCount;
    state.postsByCategory.pageCount.totalPages = response.data.totalPages;
    state.postsByCategory.pageCount.currentPage = response.data.currentPage;
    state.postsByCategory.postsLoading = false;
  },
  [CATEGORY_PURGE]: (state) => {
    state.postsByCategory.posts = [];
  },

  [NEAREST]: (state, response) => {
    state.nearest = response.data;
  },

  [PAGE_COUNT]: (state) => {
    state.pageCount.count += 1;
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
