/* eslint-disable no-shadow,no-param-reassign,no-unused-vars,no-console */
import Vue from 'vue';
import axios from 'axios';
import {
  YOUTUBE_GET,
  POST_DISLIKE,
  POST_LIKE,
} from '../actions/PostActions';
import { POST_SUCCESS } from '../actions/PostGet';

const state = { post: {}, youtube: {} };

const getters = {
  youtube: state => state.youtube,
};

const actions = {
  [POST_LIKE]: ({ commit, dispatch }, id) =>
    new Promise((resolve, reject) => {
      axios({ url: `${process.env.apiUrl}/${id}/like/`, method: 'POST' })
        .then((response) => {
          commit(POST_SUCCESS, response);
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    }),
  [POST_DISLIKE]: ({ commit, dispatch }, id) =>
    new Promise((resolve, reject) => {
      axios({ url: `${process.env.apiUrl}/${id}/dislike/`, method: 'POST' })
        .then((response) => {
          commit(POST_SUCCESS, response);
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    }),
  [YOUTUBE_GET]: ({ commit }, id) =>
    axios({ url: `${process.env.apiUrl}/youtube/${id}` })
      .then((response) => {
        commit(YOUTUBE_GET, response);
      })
      .catch((err) => {
        console.log(err);
      }),
};

const mutations = {
  [YOUTUBE_GET]: (state, response) => {
    state.youtube = response.data;
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
