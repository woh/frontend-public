/* eslint-disable no-shadow,no-param-reassign,no-console */
import axios from 'axios';
import Vue from 'vue';

import { TEASERS_REQUEST, TEASERS_SUCCESS } from '../actions/Teaser';

const state = {
  teasers: {
    teasersLoading: false,
    teasersAll: [],
    teasers: [],
    featured: [],
    teaserView: {},
    featuredView: [],
  },
};

const getters = {
  teasers: state => state.teasers,
};

const actions = {
  [TEASERS_REQUEST]: ({ commit }) => {
    commit(TEASERS_REQUEST);
    axios({ url: `${process.env.apiUrl}/teasers/all/`, method: 'GET' })
      .then((response) => {
        commit(TEASERS_SUCCESS, response);
      })
      .catch((err) => {
        console.log('Error :-S', err);
      });
  },
};

const mutations = {
  [TEASERS_REQUEST]: (state) => {
    state.teasers.teasersLoading = false;
  },

  [TEASERS_SUCCESS]: (state, response) => {
    state.teasers.teasersAll = response.data.items;

    state.teasers.teasers = state.teasers.teasersAll.filter(teaser => teaser.type === 'TYPE_TEASER');
    state.teasers.featured = state.teasers.teasersAll.filter(teaser => teaser.type === 'TYPE_FEATURE');

    Vue.set(state.teasers, 'teaserView', state.teasers.teasers[0]);
    state.teasers.featuredView = state.teasers.featured.filter((featured, index) => index < 2);
    state.teasers.teasersLoading = true;
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
