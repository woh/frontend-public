export const USER_REQUEST = 'USER_REQUEST';
export const USER_SUCCESS = 'USER_SUCCESS';
export const USER_SAVE = 'USER_SAVE';
export const USER_SAVE_PASS = 'USER_SAVE_PASS';
export const USER_ERROR = 'USER_ERROR';
export const AVATAR_SAVE = 'AVATAR_SAVE';
export const AVATAR_DEL = 'AVATAR_DEL';
