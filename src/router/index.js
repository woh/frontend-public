/* eslint-disable no-alert */
import Vue from 'vue';
import Router from 'vue-router';

// Components
import PostsList from '@/components/posts/PostsList';
import PostsByTag from '@/components/posts/PostsByTag';
import PostsByCategory from '@/components/posts/PostsByCategory';
import PostDetails from '@/components/posts/PostDetails';
import User from '@/components/user-profile/User';
import UserNewPost from '@/components/user-profile/UserNewPost';
import UserPostsList from '@/components/user-profile/UserPostsList';
import UserSettings from '@/components/user-profile/UserSettings';
import NotFoundComponent from '@/components/NotFoundComponent';
import TestPage from '@/components/TestPage';
import WoWHome from '../components/WoW/WowHome';
import store from '../store';

Vue.use(Router);

const ifAuthenticated = (to, from, next) => {
  if (store.getters.isAuthenticated) {
    next();
    return;
  }
  next('/authorize');
};

export default new Router({
  mode: 'history',
  linkActiveClass: 'active',
  routes: [
    {
      name: 'posts-list', path: '/', component: PostsList,
    },
    {
      path: '/user',
      component: User,
      meta: { menu: 'side-bar-user-page' },
      children: [
        {
          name: 'user-posts-list', path: '', component: UserPostsList, meta: { menu: 'side-bar-user-page' },
        },
        {
          name: 'user-new-post', path: '/user-new-post', component: UserNewPost, meta: { menu: 'side-bar-user-page' },
        },
        {
          name: 'user-settings', path: '/user-settings', component: UserSettings, meta: { menu: 'side-bar-user-page' },
        },
      ],
      beforeEnter: ifAuthenticated,
    },
    {
      name: 'wow-home', path: '/wow', component: WoWHome, meta: { layout: 'WowLayout' },
    },
    {
      name: 'posts-by-tag', path: '/tag/:tag', component: PostsByTag,
    },
    {
      name: 'test', path: '/test', component: TestPage,
    },
    {
      name: 'posts-by-category-sub', path: '/category-list/:category/:sub', component: PostsByCategory,
    },
    {
      name: 'posts-by-category', path: '/category-list/:category', component: PostsByCategory,
    },
    {
      name: 'post-details', path: '/category-list/:category/post/:id', component: PostDetails,
    },
    { path: '*', component: NotFoundComponent },
  ],
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    }
    return { x: 0, y: 0 };
  },
});
