import TextareaParser from '../../services/textarea-parser';
import EmojiService from '../../services/emoji-service';
import CoolPicker from '../coolpicker/coolpicker.vue';
import SendingLoading from '../../../../parts/misc/SendingLoading2';
import MediaGalleryDialog from '../../../../parts/MediaGalleryDialog';
import { mapGetters } from 'vuex';
import axios from 'axios';
import guidGenerator from '../../../../services/GuidGenerator';
import { COMMENT_ADD_REQUEST, COMMENT_ADD_TEMPORARY, COMMENT_SAVE } from '../../../../../store/actions/comments';

export default {
  name: 'CoolTextArea',
  components: {
    'coolpicker': CoolPicker,
    SendingLoading,
    MediaGalleryDialog,
  },
  props: {
    // ** Picker Props **/
    replyTo: {
      type: Object,
    },
    comment: {
      type: Object,
    },
    edit: {
      default: false,
      type: Boolean,
    },
    pickerWidth: {
      default: 250,
      type: Number,
    },
    pickerMaxHeight: {
      default: 200,
      type: Number,
    },
    appendToBody: {
      default: true,
      type: Boolean,
    },
    triggerType: {
      default: 'click',
      type: String,
      validator: function (value) {
        if (value !== 'click' && value !== 'hover') {
          console.error('The value entered for the prop "triggerType" is invalid. '+
            'Valid values: "click" and "hover".');
        }
        return true;
      }
    },
    emojiData: {
      default: () => [],
      type: Array,
    },
    emojiGroups: {
      default: () => [],
      type: Array,
    },
    skinsSelection: {
      default: false,
      type: Boolean,
    },
    recentEmojisFeat: {
      default: false,
      type: Boolean,
    },
    recentEmojisStorage: {
      default: 'none',
      type: String,
      validator: function (value) {
        if (value !== 'local' && value !== 'session' && value !== 'none') {
          console.error('The value entered for the prop "recentEmojisStorage" is invalid. '+
            'Valid values: "local", "session" and "none".');
        }
        return true;
      }
    },
    recentEmojiStorageName: {
      default: 'cep-recent-emojis',
      type: String,
    },
    recentEmojiLimit: {
      default: 12,
      type: Number,
    },
    searchEmojisFeat: {
      default: false,
      type: Boolean,
    },
    searchEmojiPlaceholder: {
      default: 'Search emojis.',
      type: String,
    },
    searchEmojiNotFound: {
      default: 'No emojis found.',
      type: String,
    },
    twemojiPath: {
      default: 'https://twemoji.maxcdn.com/2/',
      type: String
    },
    twemojiExtension: {
      default: '.png',
      type: String,
      validator: function (value) {
        let bolValid = ['.png', '.svg', '.jpg', '.jpeg', '.ico'].indexOf(value) !== -1;
        if (bolValid === false) {
          console.error('The value entered for the prop "twemojiPath" is invalid. '+
            'Valid values: ".png", ".svg", ".jpg", ".jpeg", ".ico".');
        }
        return true;
      }
    },
    twemojiFolder: {
      default: '72x72',
      type: String
    },

    // ** Textarea Props **/
    content: {
      default: '',
      type: String
    },
    enableSendBtn: {
      default: false,
      type: Boolean
    },
    disableEmojiPicker: {
      default: false,
      type: Boolean
    },
    disabled: {
      default: false,
      type: Boolean
    },
    componentColor: {
      default: 'cream',
      validator: function (value) {
        let bolValid =  ['cream', 'cherry', 'forest', 'ocean', 'sun', 'transparent'].indexOf(value) !== -1;
        if (bolValid === false) {
          console.error('The value entered for the prop "componentColor" is invalid. '+
            'Valid values: "cream", "cherry", "forest", "ocean", "sun".');
        }
        return true;
      }
    },
  },
  data() {
    return {
      savedRange: null,
      newComment: {
        createdAt: new Date().toISOString(),
        text: null,
        postId: this.$route.params.id,
        rating: { count: 0, like: null, dislike: null },
        guid: null,
      },
      url: null,
      quality: 0.4,
      commentText: '',
      newVideoItem: null,
      newImageItem: null,
      commentMedia: [],
      checkedLinkArr: [],
      placeholder: true,
      visibleDialogVideo: false,
      videoPopoverInput: '',
      submitting: false,
      index: { i: null },
      dialogVisible: { visible: false },
      visibleEmoji: false,
      eventFocusInput: 'blur',
    }
  },
  computed: {
    ...mapGetters(['user']),
    coolTextarea() {
      return this.$refs.coolTextarea;
    },
    minHeight() {
      return {
        'input-focus-height': this.commentText || this.eventFocusInput === 'focus' || this.replyTo.id,
      };
    },
    commentBodyShadow() {
      return {
        'comment-input-body-shadow': this.commentText || this.eventFocusInput === 'focus',
      }
    },
    footerVisible() {
      return {
        'input-footer-visible': this.commentText || this.eventFocusInput === 'focus',
      };
    },
  },
  created() {
    this.twemojiOptions = { base: this.twemojiPath, ext: this.twemojiExtension, size: this.twemojiFolder };
  },
  mounted() {
    if (this.edit) {
      this.coolTextarea.innerHTML = this.comment.text;
      this.onChange();
      this.commentMedia = JSON.parse(JSON.stringify(this.comment.media)) || [];
      this.endSelection();
    } else this.blur();
  },
  methods: {
    cancelComment() {
      this.eventFocusInput = null;
      this.clean();
    },
    commentInputFocus(event) {
      if (event) {
        this.eventFocusInput = event.type;
      }
    },
    onChange() {
      this.commentText = this.coolTextarea.innerHTML;
    },
    showGallery(i) {
      this.dialogVisible.visible = true;
      this.index.i = i;
    },
    commentImageUrl(img) {
      let image = img;
      if (image && image.indexOf('http://dev.woh.ru') === -1) {
        image = `${process.env.imgRoot}${image}`;
      }
      return image;
    },
    flexBasis(el) {
      return {
        'flex-basis-5': this.commentMedia.length > 5 && this.commentMedia.length < 7,
        'flex-basis-7': this.commentMedia.length > 6,
        'is-vertical': el.width < el.height,
        'is-vertical-one': el.width < el.height && this.commentMedia.length < 2,
      };
    },
    mediaElDelete(index) {
      this.checkedLinkArr.splice(this.checkedLinkArr.indexOf(this.commentMedia[index].thumbnail.src), 1);
      this.commentMedia.splice(index, 1);
    },

    getYoutubeId(url) {
      let id = null;
      if (url.indexOf('youtube.com') !== -1) {
        [id] = url.replace('https://www.youtube.com/watch?v=', '').split('&');
      } else {
        [id] = url.replace('https://youtu.be/', '').split('?');
      }
      return { id, url };
    },
    addVideoFromPopover() {
      this.addYoutubeVideo(this.getYoutubeId(this.videoPopoverInput), this.videoPopoverInput);
      this.visibleDialogVideo = false;
      this.videoPopoverInput = null;
    },
    addYoutubeVideo(item) {
      if (this.commentMedia.findIndex(x => x.url === item.id) === -1) {
        axios({ url: `${process.env.apiUrl}/youtube/${item.id}` })
          .then((response) => {
            if (response.data.items.length > 0) {
              const imgUrl = response.data.items[0].snippet.thumbnails.maxres || response.data.items[0].snippet.thumbnails.high;
              this.getImgByUrl(imgUrl.url, (canvas) => {
                const base64Image = canvas.toDataURL('image/jpeg', this.quality);
                this.newVideoItem = {
                  url: item.id,
                  title: response.data.items[0].snippet.title,
                  thumbnail: {
                    content: base64Image, src: item.url, width: canvas.width, height: canvas.height,
                  },
                  embedCode: response.data.items[0].player.embedHtml,
                };
                this.commentMedia.push(this.newVideoItem);
                this.textUrlClean(item.url);
              });
            }
          })
          .catch((err) => {
            console.log(err);
          });
      }
    },

    getImgByUrl(url, callback) {
      const img = new Image();
      img.crossOrigin = 'Anonymous';
      img.src = `${process.env.imgProxy}${url}`;
      const canvas = document.createElement('canvas');
      const ctx = canvas.getContext('2d');
      img.onload = () => {
        canvas.width = img.width;
        canvas.height = img.height;
        ctx.drawImage(img, 0, 0);
        callback(canvas, url);
      };
    },
    addImgByUrl(item) {
      if (this.commentMedia.findIndex(x => x.thumbnail.src === item) === -1) {
        this.getImgByUrl(item, (canvas, url) => {
          const base64Image = canvas.toDataURL('image/jpeg', this.quality);
          if (base64Image) {
            this.textUrlClean(item);
            this.commentMedia.push(this.newImageItem = {
              thumbnail: {
                content: base64Image,
                src: url,
                width: canvas.width,
                height: canvas.height,
              },
            });
          }
        });
      }
    },
    addImgFromFile(event) {
      if (event.target.files[0] && event.target.files[0].type.indexOf('image') !== -1) {
        const file = event.target.files[0];
        const reader = new FileReader();
        reader.onloadend = () => {
          const img = new Image();
          img.crossOrigin = 'Anonymous';
          img.src = reader.result;
          img.onload = () => {
            const canvas = document.createElement('canvas');
            canvas.width = img.width;
            canvas.height = img.height;
            const ctx = canvas.getContext('2d');
            ctx.drawImage(img, 0, 0);
            this.commentMedia.push(this.newImageItem = { thumbnail: { content: canvas.toDataURL('image/jpeg', this.quality), width: canvas.width, height: canvas.height } });
          };
        };
        reader.readAsDataURL(file);
      }
    },

    commentSend() {
      if (this.edit) {
        this.commentSave();
      } else {
        this.commentAdd();
      }
    },
    commentAdd() {
      // console.log(this.$refs.commentText.$refs.coolTextarea.nextElementSibling);
      console.log('коммент эдд', this.replyTo);
      if (this.commentText.length > 0 || this.commentMedia.length > 0) {
        this.newComment.guid = guidGenerator();
        console.log(this.commentText);
        this.newComment.text = this.commentText.trim();
        this.newComment.media = this.commentMedia;
        this.newComment.user = this.user;
        this.newComment.replyTo = Object.assign({}, this.replyTo);
        const temporary = Object.assign({}, this.newComment);
        const sendingComment = Object.assign({}, this.newComment);
        this.$store.commit(COMMENT_ADD_TEMPORARY, temporary);
        this.clean();
        this.$store.dispatch(COMMENT_ADD_REQUEST, sendingComment).then(() => {
          localStorage.removeItem(`post${this.$route.params.id}`);
        }).catch(() => {
        });
      }
    },
    commentSave() {
      this.comment.postId = this.$route.params.id;
      this.comment.text = this.commentText.replace('️', '').trim();
      this.comment.media = this.commentMedia;
      if (this.comment.text.length > 0 || this.comment.media.length > 0) {
        this.submitting = true;
        this.$store.dispatch(COMMENT_SAVE, this.comment).then(() => {
          this.submitting = false;
          this.$emit('cancelEdit');
          this.clean();
        });
      }
    },
    clean() {
      this.coolTextarea.innerHTML = '';
      this.onChange();
      this.commentMedia = [];
      this.checkedLinkArr = [];
      this.newComment.guid = null;
      this.eventFocusInput = null;
      this.replayClean();
    },
    replayClean() {
      this.replyTo.id = null;
      this.replyTo.user.name = null;
    },
    textUrlClean(url) {
      this.coolTextarea.innerHTML = this.coolTextarea.innerHTML.replace(url, '').trim();
      this.onChange();
    },
    cancelEdit() {
      this.$emit('cancelEdit');
      this.clean();
    },

    updateContent(event) {
      let content = event.target.innerHTML;
      if (content.length !== 0 && content[content.length - 1] === '\n') {
        content = content.slice(0, -1);
      }
      this.$emit('update:content', content);
      this.$emit('contentChanged');
      this.commentText = content;
    },
    emitEnterKeyEvent(event) {
      this.$emit('enterKey', event);
    },
    enterKey(event) {
      event.stopPropagation();
      event.preventDefault();
      if (event.shiftKey === false) {
        this.emitEnterKeyEvent(event);
        this.commentSend();
      }
    },
    shiftEnterKey(event) {
      event.stopPropagation();
      event.preventDefault();

      if (this.coolTextarea.innerHTML === '' ||
        this.coolTextarea.innerHTML[this.coolTextarea.innerHTML.length -1] !== '\n') {
        this.addText('\n');
        this.addText('\n');
      } else {
        this.addText('\n');
      }

      this.coolTextarea.scrollTop = this.coolTextarea.scrollHeight;
    },
    onPaste(pasteEvent) {
      var clipboardData, pastedData;

      pasteEvent.stopPropagation();
      pasteEvent.preventDefault();

      clipboardData = pasteEvent.clipboardData || window.clipboardData;
      pastedData = clipboardData.getData('Text');
      pastedData = TextareaParser.escapeHTML(pastedData);
      pastedData = EmojiService.getEmojiImgFromUnicode(pastedData, this.twemojiOptions);

      window.document.execCommand('insertHTML', false, pastedData);

      this.coolTextarea.scrollTop = this.coolTextarea.scrollHeight;
    },
    focus() {
      const doc = this.coolTextarea;
      const childNode = doc.childNodes[0];
      doc.focus();

      if (childNode === undefined) {
        const textNode = document.createTextNode('');
        doc.appendChild(textNode);
        const range = document.createRange();
        const sel = window.getSelection();
        range.setStart(doc.childNodes[0], 0);
        range.collapse(true);
        sel.removeAllRanges();
        sel.addRange(range);
        this.saveSelection();
      }
    },
    blur() {
      const doc = this.coolTextarea;
      doc.blur();
    },
    endSelection() {
      const doc = this.coolTextarea;
      const range = document.createRange();
      range.selectNodeContents(doc);
      range.collapse(false);
      const sel = window.getSelection();
      sel.removeAllRanges();
      sel.addRange(range);
    },
    saveSelection() {
      if (window.getSelection) {
        this.savedRange = window.getSelection().getRangeAt(0);
      } else if (document.selection) {
        this.savedRange = document.selection.createRange();
      }
    },
    restoreSelection() {
      const doc = this.coolTextarea;
      doc.focus();
      if (this.savedRange != null) {
        if (window.getSelection)  {
          const s = window.getSelection();
          if (s.rangeCount > 0) {
            s.removeAllRanges();
          }
          s.addRange(this.savedRange);
        } else if (document.createRange)  {
          window.getSelection().addRange(this.savedRange);
        } else if (document.selection)  {
          this.savedRange.select();
        }
      }
    },

    addTextBlur(text) {
      this.focus();

      text = TextareaParser.escapeHTML(text);
      text = EmojiService.getEmojiImgFromUnicode(text, this.twemojiOptions);

      window.document.execCommand('insertHTML', false, text);
      this.saveSelection();
      this.blur();
    },
    addText(text) {
      this.focus();

      text = TextareaParser.escapeHTML(text);
      text = EmojiService.getEmojiImgFromUnicode(text, this.twemojiOptions);

      window.document.execCommand('insertHTML', false, text);
      this.saveSelection();
    },
    cleanText() {
      this.coolTextarea.innerHTML = '';
      this.$emit('update:content', '');
    },
    emojiUnicodeAdded(unicode) {
      this.$emit('emojiUnicodeAdded', unicode);
    },
    emojiImgAdded(img) {
      this.$emit('emojiImgAdded', img);
    }
  },
  watch: {
    commentText(commentText) { // ловим урлы
      if (commentText.match(/http(?:[:?&=a-zA-Z0-9_\-./#]*)/g)) {
        commentText.match(/http(?:[:?&=a-zA-Z0-9_\-./#]*)/g).forEach((url) => {
          if (this.checkedLinkArr.indexOf(url) === -1 && url.indexOf('https://twemoji.maxcdn.com') === -1) this.checkedLinkArr.push(url);
        });
      }
    },
    checkedLinkArr(checkedLinkArr) { // получаем картинки и видео из урлов
      if (checkedLinkArr) {
        checkedLinkArr.forEach((item) => {
          if (item.indexOf('youtube.com') !== -1 || item.indexOf('youtu.be') !== -1) {
            this.addYoutubeVideo(this.getYoutubeId(item), item);
          } else {
            this.addImgByUrl(item);
          }
        });
      }
    },
  },
};
