import axios from 'axios';

async function getImageFromVideo(item) {
  const { data } = await axios({ url: `${process.env.apiUrl}/youtube/${item}` });
  return data.items[0].snippet.thumbnails.maxres.url || data.items[0].snippet.thumbnails.high.url;
}

export default function getImages(post) {
  const images = [];
  post.text.match(/src="(http(?:[:?&=a-zA-Z0-9_\-./#]*))/g)
    .forEach((url, index) => {
      const img = {};
      const link = url.replace('src="', '');
      if (link.indexOf('youtube.com') !== -1) {
        const [id] = link.replace('https://www.youtube.com/embed/', '')
          .split('?');
        getImageFromVideo(id)
          .then((imgUrl) => {
            if (imgUrl) {
              img.id = index;
              img.src = imgUrl;
              img.url = `${process.env.imgProxy}${imgUrl}`;
              images.push(img);
            }
          });
      } else {
        img.id = index;
        img.src = link;
        img.url = `${process.env.imgProxy}${link}`;
        images.push(img);
      }
    });
  return images;
}
