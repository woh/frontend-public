module.exports = {
  NODE_ENV: '"production"',
  apiUrl: "'/api'",
  imgRoot: process.env.IMG_ROOT || "''",
  imgProxy: process.env.IMG_PROXY || "'http://dev.woh.ru:3003/'"
}
