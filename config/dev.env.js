var merge = require('webpack-merge')
var prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  apiUrl: process.env.API_URL || "'http://dev.woh.ru/api'",
  imgRoot: process.env.IMG_ROOT || "'http://dev.woh.ru'",
  imgProxy: process.env.IMG_PROXY || "'http://dev.woh.ru:3003/'"
})
